"""Voxelize a mesh and gernate bricks based on these voxels
    Inputs:
        m (mesh): The mesh to voxelize. Make sure the mesh is in the positive area
        Sx (float): X size of 1 voxel
        Sy (float): T size of 1 voxel
        Sz (float): Z size of 1 voxel
    Output:
        out_voxel_pnts (list of points): Centre points of the voxels
        out_bricks (list of boxes): The generated bricks"""

__author__ = "Jeroen de Bruijn J.P.deBruijn@student.tudelft.nl"
__version__ = "2018.10.18"



##### INPUTS

# Import rhino
import Rhino.Geometry as rg
# Import math functions
from math import ceil
from math import floor
from math import sqrt
# Import copy to later on create a deepcopy of a list
from copy import deepcopy



##### FUNCTIONS

# Place voxels in bounding box
def BBoxToVoxels(ZBBox, vSize):
    # This function is written based on code from the following paper:
    # Nourian, P., Gonçalves, R., Zlatanova, S., Ohori, K. A., & Vu Vo, A. (2016). Voxelization algorithms for geospatial applications: Computational methods for voxelating spatial datasets of 3D city models containing 3D surface, curve and point data models. MethodsX, 3, 69-86. doi:https://doi.org/10.1016/j.mex.2016.01.001
    
    # Get max number of voxels in all directions
    Imax = int(abs(ZBBox.Diagonal.X/vSize.X))
    Jmax = int(abs(ZBBox.Diagonal.Y/vSize.Y))
    Kmax = int(abs(ZBBox.Diagonal.Z/vSize.Z))
    
    # Calculate point shift
    shift = rg.Vector3d(ZBBox.Min) + 0.5 * vSize
    
    # Create empty list
    VoxelPoints = []
    # Loop first over the maximum amount in Z axis so the list can be selected per Z level
    # Skip the first row, since the bottom of the mesh is not perfectly straight
    for k in range(1, Kmax):
        level_list = []
        for j in range(Jmax):
            row_list = []
            for i in range(Imax):
                # Calculate point and add it to the list
                row_list.append( rg.Point3d(i * vSize.X, j * vSize.Y, k * vSize.Z) + shift )
            
            # Add row list to level list
            level_list.append( row_list )
        
        # Add level list to overall list
        VoxelPoints.append( level_list )
    
    # Return list
    return VoxelPoints

# Calculate which points are close to the surface
def NearPoints(locs, mesh, vSize, tol):
    # Define distance
    distance = vSize.Length
    
    # Loop trough the items in the list
    for i, list2d in enumerate(locs):
        for j, list1d in enumerate(list2d):
            for k, not_needed in enumerate(list1d):
                # Get point 1
                p1 = locs[i][j][k]
                # Get point on mesh
                p2 = mesh.ClosestPoint(p1)
                
                # Calculate distance
                dist = [p1.X - p2.X, p1.Y - p2.Y, p1.Z - p2.Z]
                # Normalize the distance
                norm = sqrt( (dist[0]**2) + (dist[1]**2) + (dist[2]**2) )
                
                 # Check if the distance is larger than the input distance
                if norm > distance * tol:
                    # Set boolean instead of point so later on it can be checked if the neigbouring voxels exist
                    locs[i][j][k] = False
    
    # Return result
    return locs

# Extend bounding box to size of voxel
def RBBox_to_ZBBox(RBbox, vSize):
    # This function is written based on code from the following paper:
    # Nourian, P., Gonçalves, R., Zlatanova, S., Ohori, K. A., & Vu Vo, A. (2016). Voxelization algorithms for geospatial applications: Computational methods for voxelating spatial datasets of 3D city models containing 3D surface, curve and point data models. MethodsX, 3, 69-86. doi:https://doi.org/10.1016/j.mex.2016.01.001
    
    # Convert RBBox to match voxel size
    ZMinP = BoundRP_to_ZP(RBBox.Min, vSize, False)
    ZMaxP = BoundRP_to_ZP(RBBox.Max, vSize, True)
    
    # Return bounding box
    return rg.Box(rg.Plane.WorldXY, [ZMinP, ZMaxP]).BoundingBox

# Round down or up to voxel grid
def BoundRP_to_ZP(RPoint, vSize, RoundUp):
    # This function is written based on code from the following paper:
    # Nourian, P., Gonçalves, R., Zlatanova, S., Ohori, K. A., & Vu Vo, A. (2016). Voxelization algorithms for geospatial applications: Computational methods for voxelating spatial datasets of 3D city models containing 3D surface, curve and point data models. MethodsX, 3, 69-86. doi:https://doi.org/10.1016/j.mex.2016.01.001
    
    # Get coordinates of the point
    x = RPoint.X
    y = RPoint.Y
    z = RPoint.Z
    # Get coordinates of the direction
    u = vSize.X
    v = vSize.Y
    w = vSize.Z
    
    # Check boolean var
    if RoundUp:
        # Calculate values, round up and add 2 steps in the horizontal directions
        ZPx = ceil(u * x/u) + (2*u)
        ZPy = ceil(v * y/v) + (2*v)
        ZPz = ceil(w * z/w)
    else:
        # Calculate values, round down and substract 2 steps in the horizontal directions
        ZPx = floor(u * x/u) - (2*u)
        ZPy = floor(v * y/v) - (2*v)
        ZPz = floor(w * z/w)
    
    # Return the new point which is adjusted to the voxel size grid
    return rg.Point3d( ZPx, ZPy, ZPz )

# Place bricks on a list of lists of points
def PlaceBricks(layer, br_Zval, rotateData):
    # Create a deep copy of the original list so points in the voxel grid can be set to False without causing loss of the original data
    temp_layer = deepcopy(layer)
    
    # Check if the data needs to be rotated
    if rotateData:
        # Rotate the data structure so the bricks will be placed in a different direction
        temp_layer = map(list, zip(*temp_layer))
    
    # Create empty list to hold the boxes or bricks
    boxes = []
    # Define vector for brick height
    brZ = rg.Vector3d(0, 0, br_Zval)
    
    # Create list of neighbors like below, where vp is the first voxel point and nX a neighbor
    # n12 n13 n14
    # n9  n10 n11
    # n6  n7  n8
    # n3  n4  n5
    # vp  n1  n2
    
    # Loop trough each row in the layer, exclude the last two rows to prevent index out of range error
    for u, whole_row in enumerate(temp_layer[:-2]):
        # Create variable for row, exclude the last two voxel points to prevent index out of range error
        row = whole_row[:-2]
        # Loop trough each voxel point in the row
        for v, vp in enumerate(row):
            # Check if vp is True, so it is not set to False but it contains a point
            if vp:
                # Create variables for direct neighbors which are always defined
                n1 = temp_layer[u+1][v]
                n2 = temp_layer[u+2][v]
                n3 = temp_layer[u][v+1]
                n4 = temp_layer[u+1][v+1]
                n5 = temp_layer[u+2][v+1]
                
                # Check if the next voxels are within the limits of the list
                if v+2 <= len(row):
                    n6 = temp_layer[u][v+2]
                    n7 = temp_layer[u+1][v+2]
                    n8 = temp_layer[u+2][v+2]
                else:
                    n6 = False
                    n7 = False
                    n8 = False
                
                if v+3 <= len(row):
                    n9 = temp_layer[u][v+3]
                    n10 = temp_layer[u+1][v+3]
                    n11 = temp_layer[u+2][v+3]
                else:
                    n9 = False
                    n10 = False
                    n11 = False
                
                if v+4 <= len(row):
                    n12 = temp_layer[u][v+4]
                    n13 = temp_layer[u+1][v+4]
                    n14 = temp_layer[u+2][v+4]
                else:
                    n12 = False
                    n13 = False
                    n14 = False
                
                # Check if the furthest neighbor is True to create a full brick
                # And check the neighbors in between
                if n14 and n1 and n2 and n3 and n4 and n5 and n6 and n7 and n8 and n9 and n10 and n11 and n12 and n13:
                    # Create box and add it to list
                    boxes.append( rg.Box(rg.Plane.WorldXY, (vp, n14 + brZ)) )
                    
                    # Set points to false to prevent creating another brick on these points
                    temp_layer[u][v] = False
                    temp_layer[u][v+1] = False
                    temp_layer[u][v+2] = False
                    temp_layer[u][v+3] = False
                    temp_layer[u+1][v] = False
                    temp_layer[u+1][v+1] = False
                    temp_layer[u+1][v+2] = False
                    temp_layer[u+1][v+3] = False
                # Else check the second furthest neighbor to create a 3/4 brick
                # And check the neighbors in between
                elif n11 and n1 and n2 and n3 and n4 and n5 and n6 and n7 and n8 and n9 and n10 :
                    # Create box
                    boxes.append( rg.Box(rg.Plane.WorldXY, (vp, n11 + brZ)) )
                    
                    # Set points to false to prevent creating another brick on these points
                    temp_layer[u][v] = False
                    temp_layer[u][v+1] = False
                    temp_layer[u][v+2] = False
                    temp_layer[u+1][v] = False
                    temp_layer[u+1][v+1] = False
                    temp_layer[u+1][v+2] = False
                # Else check the third furthest neighbor to create a 1/2 brick
                # And check the neighbors in between
                elif n8 and n1 and n2 and n3 and n4 and n5 and n6 and n7:
                    # Create box
                    boxes.append( rg.Box(rg.Plane.WorldXY, (vp, n8 + brZ)) )
                    
                    # Set points to false to prevent creating another brick on these points
                    temp_layer[u][v] = False
                    temp_layer[u][v+1] = False
                    temp_layer[u+1][v] = False
                    temp_layer[u+1][v+1] = False
    
    # Return list of boxes
    return boxes



##### MAIN CODE

# Set relative tolerance to add more points which are close to the mesh
multiplyDistance = 1.5

# Create voxel size as a vector
vSize = rg.Vector3d(Sx, Sy, Sz)

# Create bounding box around input
RBBox = m.GetBoundingBox(True)
# Update bounding box to match boxel size
ZBBox = RBBox_to_ZBBox(RBBox, vSize)
# Create voxel points inside bounding box
voxelList = BBoxToVoxels(ZBBox, vSize)
# Get only the points which are close enough to the mesh surface
voxelPnts = NearPoints(voxelList, m, vSize, multiplyDistance)

# Empty list
allBricks = []
# Loop trough each horizontal layer of all the voxel points
for count, singleLayer in enumerate(voxelPnts):
    # Check if the counter is even
    if count % 2 == 0:
        # Place bricks on the layer and rotate the data structure
        brks = PlaceBricks(singleLayer, Sz, True)
    else:
        # Place bricks on the layer
        brks = PlaceBricks(singleLayer, Sz, False)
    
    # Add bricks to the list
    allBricks.append(brks)



### OUTPUT

# Show all bricks
out_bricks = allBricks
out_voxel_pnts = voxelPnts